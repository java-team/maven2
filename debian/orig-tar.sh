#!/bin/sh -e

VERSION=$2
TAR=../maven2_$VERSION.orig.tar.gz
DIR=maven2-$VERSION
TAG=$(echo maven-$2 | sed s/~RC/-RC/ )

svn export http://svn.apache.org/repos/asf/maven/maven-2/tags/$TAG $DIR
GZIP=--best tar -c -z -f $TAR -X debian/orig-tar.exclude $DIR
rm -rf $DIR ../$TAG

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir
  echo "moved $TAR to $origDir"
fi
